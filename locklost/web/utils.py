import os
from datetime import datetime

from .. import config
from ..event import find_events

##################################################

def event_gen(query):
    """generate events from query

    """
    for i, event in enumerate(find_events(**query)):
        # limit of 0 is no limit
        if bool(query.get('limit', 0)) and i >= query['limit']:
            break
        yield event


def analysis_status_button(event):
    """HTML-formated event analysis status button

    """
    if event.analyzing:
        fflag = "analyzing"
        state = 'warning'
    elif event.analyzed:
        if event.analysis_succeeded:
            fflag = "analyzed [{}]".format(event.analysis_version)
            state = 'success'
        else:
            fflag = "fail"
            state = 'danger'
    else:
        fflag = "queued"
        state = 'warning'
    log_link = event.url('log')
    return '<a href="{log_link}" class="btn-sm btn-{state} btn-block" role="button" style="text-align:center;">{fflag}</a>'.format(
        log_link=log_link,
        state=state,
        fflag=fflag,
    )


def tag_buttons(tags):
    """HTML-formated tag list

    """
    buttons = []
    for tag in tags:
        colors = config.TAG_COLORS.get(tag, ('white', 'grey'))
        buttons.append(
            '<span style="color:{}; background-color:{}">{}</span>'.format(
                colors[0], colors[1], tag))
    return ' '.join(buttons)


def event_plot_urls(event, plot):
    """event plot urls

    """
    plot_urls = []
    for zoom_level in ['WIDE', 'ZOOM']:
        plot_name = '{}_{}.png'.format(plot, zoom_level)
        plot_path = event.path(plot_name)
        if os.path.exists(plot_path):
            plot_urls.append(event.url(plot_name))
    return plot_urls


def gen_thumbnail_url(plot_url):
    """thumbnail url for plot given url

    """
    url_start = '/'.join(plot_url.split('/')[0:-1])
    url_end = plot_url.split('/')[-1].split('.')[0]+'_thumbnail.png'
    return url_start+'/thumbnails/'+url_end
