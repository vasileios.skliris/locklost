% rebase('base.tpl', IFO=IFO, web_script=web_script, online_status=online_status)

% import os

% from locklost import config

<!-- Summary title -->
<h4>Summary Plots</h4>

<!-- Plot tabs -->
<button class="tablink" onclick="openPage('run', this, 'orange')">Run</button>
<button class="tablink" onclick="openPage('month', this, 'green')">Month</button>
<button class="tablink" onclick="openPage('week', this, 'blue')" id="defaultOpen">Week</button>

% for id in ['run', 'month', 'week']:
%   plot_dir = os.path.join(config.EVENT_ROOT, 'summary_plots', id)
%   plotnames = os.listdir(plot_dir)
    <div id={{id}} class="tabcontent">
%   for plotname in plotnames:
%     plot_url = os.path.join(config.WEB_ROOT, 'events/summary_plots', id, plotname)
      <hr />
      <div class="container">
      <div class="row">
      % include('plots.tpl', plots=[plot_url], size=8)
      </div>
      </div>
%   end
    </div>
% end

<script>
function openPage(pageName, elmnt, color) {
  // Hide all elements with class="tabcontent" by default */
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Remove the background color of all tablinks/buttons
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }

  // Show the specific tab content
  document.getElementById(pageName).style.display = "block";

  // Add the specific color to the button used to open the tab content
  elmnt.style.backgroundColor = color;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
