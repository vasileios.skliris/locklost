import logging
import requests
import datetime
import subprocess

import gpstime

from .. import config


GUARDLOG_WINDOW = (-120, 1)
GUARDLOG_NODES = config.GRD_NODE


def retrieve_guardlog(event):
    """Retrieve guardian log around time of event.

    REQUIRES ACCESS TO GUARDIAN LOGS (currently only available in CDS network).

    """
    time = gpstime.gpstime.fromgps(event.transition_gps)
    window = GUARDLOG_WINDOW
    after = time + datetime.timedelta(seconds=window[0])
    before = time + datetime.timedelta(seconds=window[1])
    cmd = [
        'guardctrl', '+t', 'log',
        '--after', after.isoformat(),
        '--before', before.isoformat(),
        '--lines', '1000000',
        GUARDLOG_NODES,
    ]
    logging.info("retrieving guardian log...")
    logging.debug(' '.join(cmd))
    with open(event.path('guardlog.txt'), 'w') as f:
        subprocess.call(
            cmd,
            stdout=f,
            universal_newlines=True,
        )


def previous_state_name(event):
    """Find name of previous lock state.

    REQUIRES ACCESS TO GUARDIAN USERCODE

    """
    from guardian import system
    sys = system.GuardSystem(config.GRD_NODE)
    sys.load()
    state_index = event.transition_index[0]
    name = sys.index(state_index)
    logging.info("previous guardian state name: {}".format(name))
    with open(event.path('previous_state_name'), 'w') as f:
        f.write(name+'\n')


def fetch_cds_guardinfo(event):
    """Fetch guardian info from CDS.

    """
    URL_BASE = os.path.join(config.CDS_EVENT_ROOT, event.epath)

    r = requests.get(URL_BASE+'guardlog.txt')
    with open(event.path('guardlog.txt'), 'w') as f:
        f.write(r.text)

    r = requests.get(URL_BASE+'previous_state_name')
    with open(event.path('previous_state_name'), 'w') as f:
        f.write(r.text)


def guardian_info(event):
    """Retrieve all guardian info.

    """
    retrieve_guardlog(event)
    previous_state_name(event)
