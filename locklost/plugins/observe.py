from gwpy.segments import Segment

from .. import config
from .. import data

##################################################

def check_observe(event):
    """Observe status at time of lock loss.

    """
    # use transition gps since that doesn't depend on refinement
    gps = event.transition_gps

    window = [-1, 0]
    segment = Segment(*window).shift(gps)

    buf = data.fetch([config.IFO+':GRD-IFO_OK'], segment)[0]

    if bool(buf.data[0]):
        event.add_tag('OBSERVE')
