import logging

from gwpy.segments import Segment

from .. import config
from .. import data

WINDOW = [-3, 0]
CHANNEL = ['{}:GRD-SEI_BS_STATE_N'.format(config.IFO)]

##############################################

def check_sei_bs(event):
    """Checks if SEI_BS Guardian node is in transition state during lockloss and
    creates a tag."""

    if config.IFO == 'L1':
        logging.info("followup is not configured or not applicable to LLO.")
        return

    mod_window = [WINDOW[0], WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    stage_2_channel = data.fetch(CHANNEL, segment)[0]

    if any(stage_2_channel.data == -19):
        event.add_tag('SEI_BS_TRANS')
    else:
        logging.info('SEI BS was not transitioning during lockloss')
