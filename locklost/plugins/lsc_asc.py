import numpy as np
import matplotlib.pyplot as plt
import os

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

##############################################

def plot_lsc_asc(event):
    """Plot LSC/ASC channels

    """
    gps = event.gps
    plotutils.set_rcparams()
    os.mkdir(event.path('thumbnails'))

    main_fig, main_ax = plt.subplots(1, figsize=(22, 16))
    main_fig.tight_layout()

    thumb_fig, thumb_ax = plt.subplots(1, figsize=(11, 8))
    thumb_fig.tight_layout()

    for chan_group, channels in config.LSC_ASC_CHANNELS.items():
        for window_type, window in config.PLOT_WINDOWS.items():
            segment = Segment(window).shift(gps)
            bufs = data.fetch(channels, segment)
            for idx, buf in enumerate(bufs):
                name = buf.channel

                gen_plot(main_ax, buf, event, window, name)
                outfile_plot = '{}_{}.png'.format(name, window_type)
                outpath_plot = event.path(outfile_plot)
                main_fig.savefig(outpath_plot, bbox_inches='tight')
                main_ax.clear()

                gen_plot(thumb_ax, buf, event, window, name)
                outfile_plot = '{}_{}_{}.png'.format(name, window_type, 'thumbnail')
                outpath_plot = event.path('thumbnails', outfile_plot)
                thumb_fig.savefig(outpath_plot, bbox_inches='tight')
                thumb_ax.clear()


def gen_plot(ax, buf, event, window, name):
    color = 'royalblue'
    srate = buf.sample_rate
    t = np.arange(window[0], window[1], 1/srate)
    if event.refined:
        status = 'refined'
    else:
        status = 'unrefined'
    ax.set_xlabel('Time [s] since {} lock loss at {}'.format(status, event.gps),
                  labelpad=10)
    ax.set_ylabel('Counts')
    ax.set_xlim(window)
    ymin, ymax = calc_ylims(t, buf.data)
    ax.set_ylim(ymin, ymax)
    ax.grid()
    ax.plot(t, buf.data, color=color, linewidth=2.5)
    ax.set_title(name)


def calc_ylims(t, y):
    scaling_times = np.where(t < 0)[0]
    scaling_vals = y[scaling_times]
    ymin, ymax = min(scaling_vals), max(scaling_vals)

    if ymax > 0:
        ymax = 1.1*ymax
    else:
        ymax = 0.9*ymax
    if ymin < 0:
        ymin = 1.1*ymin
    else:
        ymin = 0.9*ymin

    if ymin == ymax:
        ymin, ymax = -0.2, 0.2

    return ymin, ymax
